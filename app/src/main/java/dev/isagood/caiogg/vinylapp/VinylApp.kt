package dev.isagood.caiogg.vinylapp

import io.realm.kotlin.mongodb.App
import android.app.Application
import android.util.Log
import io.realm.kotlin.mongodb.AppConfiguration

lateinit var app: App

class VinylApp:Application(){
    override fun onCreate() {
        super.onCreate()
        app = App.create(
            AppConfiguration.Builder(getString(R.string.realm_app_id)).baseUrl(getString(R.string.realm_base_url)).build()
        )
    }
}