package dev.isagood.caiogg.vinylapp.viewmodel

import android.os.Bundle
import androidx.lifecycle.AbstractSavedStateViewModelFactory
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import androidx.savedstate.SavedStateRegistryOwner
import com.google.android.gms.maps.model.LatLng
import dev.isagood.caiogg.vinylapp.data.RealmSyncRepository
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class AddVinylViewModel (
    private val repository: RealmSyncRepository,
) : ViewModel() {

    fun addVinyl(title: String, latLng: LatLng, description: String, image: ByteArray ){
            CoroutineScope(Dispatchers.IO).launch {
                repository.addVinyl(title, latLng.longitude, latLng.latitude, description, image)
            }

    }

    @Suppress("UNCHECKED_CAST")
    companion object {
        fun factory(
            repository: RealmSyncRepository,
            owner: SavedStateRegistryOwner,
            defaultArgs: Bundle? = null,
        ): AbstractSavedStateViewModelFactory {
            return object : AbstractSavedStateViewModelFactory(owner, defaultArgs) {
                override fun <T : ViewModel> create(
                    key: String,
                    modelClass: Class<T>,
                    handle: SavedStateHandle,
                ): T {
                    return AddVinylViewModel(repository) as T
                }
            }
        }
    }
}