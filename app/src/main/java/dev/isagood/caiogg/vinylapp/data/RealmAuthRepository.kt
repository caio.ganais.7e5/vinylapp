package dev.isagood.caiogg.vinylapp.data

import dev.isagood.caiogg.vinylapp.app
import io.realm.kotlin.mongodb.Credentials

object RealmAuthRepository{
    suspend fun createAccount(email: String, password: String) {
        app.emailPasswordAuth.registerUser(email, password)
    }

    suspend fun login(email: String, password: String) {
        app.login(Credentials.emailPassword(email, password))
    }
}