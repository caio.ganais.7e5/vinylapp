package dev.isagood.caiogg.vinylapp.ui.login

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.widget.addTextChangedListener
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.NavHostFragment.findNavController
import androidx.navigation.fragment.findNavController
import dev.isagood.caiogg.vinylapp.R
import dev.isagood.caiogg.vinylapp.databinding.FragmentLoginBinding
import dev.isagood.caiogg.vinylapp.ui.map.MapActivity
import dev.isagood.caiogg.vinylapp.viewmodel.LoginAction
import dev.isagood.caiogg.vinylapp.viewmodel.LoginViewModel
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch

class LoginFragment : Fragment() {
    private var _binding: FragmentLoginBinding? = null

    private val binding get() = _binding!!
    private val viewModel: LoginViewModel by activityViewModels()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentLoginBinding.inflate(inflater, container, false)
        return binding.root

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.newUser.setOnClickListener{
            findNavController().navigate(R.id.action_loginFragment_to_registerFragment)
        }
        viewModel.switchToAction(LoginAction.LOGIN)
        val mail = binding.mail
        val password = binding.password
        val loginButton = binding.loginButton
        lifecycleScope.launch {
            viewModel.state.collect{state ->
                with(mail){
                    isEnabled = state.enabled
                    addTextChangedListener {
                        viewModel.setEmail(it.toString())
                    }
                }
                with(password){
                    isEnabled = state.enabled
                    addTextChangedListener {
                        viewModel.setPassword(it.toString())
                    }
                }
                with(loginButton){
                    isEnabled = state.enabled
                    setOnClickListener {
                        viewModel.login(state.email, state.password)
                    }
                }
            }
        }

        lifecycleScope.launch {
            viewModel.event.collect{event ->
                Toast.makeText(context, event.message, Toast.LENGTH_SHORT).show()
                if (!event.error){
                    val intent = Intent(activity, MapActivity::class.java)
                    startActivity(intent)
                    activity?.finish()
                }
            }
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}