package dev.isagood.caiogg.vinylapp.domain

import io.realm.kotlin.types.RealmObject
import io.realm.kotlin.types.annotations.PersistedName
import io.realm.kotlin.types.annotations.PrimaryKey
import org.mongodb.kbson.ObjectId

open class Vinyl(
    var description: String,
    @PrimaryKey
    @PersistedName("_id")
    var id: ObjectId,
    var latitude: Double,
    var longitude: Double,
    @PersistedName("owner_id")
    var ownerId: String,
    var title: String,
    var image: ByteArray?
) : RealmObject {
    constructor() : this(
        description = "",
        id = ObjectId(),
        latitude = 0.0,
        longitude = 0.0,
        ownerId = "",
        title = "",
        image= null
    )
}