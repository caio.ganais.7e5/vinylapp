package dev.isagood.caiogg.vinylapp.ui.map

import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.lifecycle.lifecycleScope
import dev.isagood.caiogg.vinylapp.R
import dev.isagood.caiogg.vinylapp.app
import dev.isagood.caiogg.vinylapp.ui.login.LoginActivity
import kotlinx.coroutines.launch


class LogoutFragment : Fragment() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        lifecycleScope.launch {
            kotlin.runCatching {
                app.currentUser?.logOut()

            }
                .onSuccess {
                    startActivity(Intent(activity?.applicationContext, LoginActivity::class.java))
                    activity?.finish()
                }
                .onFailure {
                    Toast.makeText(context,"ERROR",Toast.LENGTH_SHORT).show()
                }
        }
        startActivity(Intent(activity, LoginActivity::class.java))
        activity?.finish()
    }


}