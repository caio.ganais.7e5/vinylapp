package dev.isagood.caiogg.vinylapp.viewmodel

import androidx.lifecycle.ViewModel
import dev.isagood.caiogg.vinylapp.app
import dev.isagood.caiogg.vinylapp.data.RealmAuthRepository
import io.realm.kotlin.mongodb.Credentials
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.launch


data class GoToMap(val message: String, val error: Boolean)

enum class LoginAction {
    LOGIN, CREATE_ACCOUNT
}

data class LoginState(
    val action: LoginAction,
    val email: String = "",
    val password: String = "",
    val enabled: Boolean = true
) {
    companion object {

        val initialState = LoginState(action = LoginAction.LOGIN)
    }
}

class LoginViewModel : ViewModel() {
    private val _event: MutableSharedFlow<GoToMap> = MutableSharedFlow()
    val event: Flow<GoToMap>
        get() = _event

    private val _state: MutableStateFlow<LoginState> = MutableStateFlow(LoginState.initialState)
    val state: StateFlow<LoginState>
        get() = _state

    private val authRepository: RealmAuthRepository = RealmAuthRepository

    fun switchToAction(loginAction: LoginAction) {
        _state.value = state.value.copy(action = loginAction)
    }

    fun setEmail(email: String) {
        _state.value = state.value.copy(email = email)
    }

    fun setPassword(password: String) {
        _state.value = state.value.copy(password = password)
    }
    fun createAccount(email: String, password: String) {
        _state.value = state.value.copy(enabled = false)

        CoroutineScope(Dispatchers.IO).launch {
            runCatching {
                authRepository.createAccount(email, password)
            }.onSuccess {
                login(email, password)
            }.onFailure { ex: Throwable ->
                _state.value = state.value.copy(enabled = true)
            }
        }
    }

    fun login(email: String, password: String, fromCreation: Boolean = false) {
        if (!fromCreation) {
            _state.value = state.value.copy(enabled = false)
        }

        CoroutineScope(Dispatchers.IO).launch {
            runCatching {
                authRepository.login(email, password)
            }.onSuccess {
                _event.emit(GoToMap("Success!", false))
            }.onFailure { ex: Throwable ->
                _event.emit(GoToMap(ex.toString(), true))
                _state.value = state.value.copy(enabled = true)
            }
        }
    }
}