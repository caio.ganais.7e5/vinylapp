package dev.isagood.caiogg.vinylapp.data

import dev.isagood.caiogg.vinylapp.app
import io.realm.kotlin.Realm
import io.realm.kotlin.mongodb.User
import io.realm.kotlin.mongodb.subscriptions
import io.realm.kotlin.mongodb.sync.SyncConfiguration
import io.realm.kotlin.notifications.ResultsChange
import io.realm.kotlin.query.Sort
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.launch
import dev.isagood.caiogg.vinylapp.domain.Vinyl
import io.realm.kotlin.ext.query
import io.realm.kotlin.query.RealmQuery

class RealmSyncRepository() {

    private val realm: Realm
    private val config: SyncConfiguration
    private val currentUser: User
        get() = app.currentUser!!

    init {
        config = SyncConfiguration.Builder(currentUser, setOf(Vinyl::class))
            .initialSubscriptions(rerunOnOpen = true) { realm ->
                add(getQuery(realm),
                    updateExisting = true)
            }
            .waitForInitialRemoteData()
            .build()

        realm = Realm.open(config)

        CoroutineScope(Dispatchers.Main).launch {
            realm.subscriptions.waitForSynchronization()
        }
    }

    fun getVinylList(): Flow<ResultsChange<Vinyl>> {
        return realm.query<Vinyl>()
            .sort(Pair("_id", Sort.ASCENDING))
            .asFlow()
    }

    suspend fun addVinyl(
        annotationTitle: String,
        annotationLongitude: Double,
        annotationLatitude: Double,
        annotationDescription: String,
        annotationByteArray: ByteArray
    ) {
        val vinyl = Vinyl().apply {
            ownerId = currentUser.id
            title = annotationTitle
            longitude = annotationLongitude
            latitude = annotationLatitude
            description = annotationDescription
            image = annotationByteArray
        }
        realm.write {
            copyToRealm(vinyl)
        }
    }



    private fun getQuery(realm: Realm): RealmQuery<Vinyl> =
        realm.query("owner_id == $0", currentUser.id)

}

