package dev.isagood.caiogg.vinylapp.ui.map

import android.app.Activity
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.location.Location
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.activity.viewModels
import androidx.core.app.ActivityCompat
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.Marker
import com.google.android.gms.maps.model.MarkerOptions
import dev.isagood.caiogg.vinylapp.R
import dev.isagood.caiogg.vinylapp.databinding.FragmentMapBinding
import dev.isagood.caiogg.vinylapp.viewmodel.AddVinylViewModel
import dev.isagood.caiogg.vinylapp.viewmodel.LoginViewModel
import dev.isagood.caiogg.vinylapp.viewmodel.VinylViewModel
import kotlinx.coroutines.launch


lateinit var map: GoogleMap


class MapFragment : Fragment(), OnMapReadyCallback, GoogleMap.OnMarkerClickListener {
    private var _binding: FragmentMapBinding? = null
    lateinit var myMarker: MarkerOptions
    private lateinit var lastLocation: Location
    private lateinit var fusedLocationClient: FusedLocationProviderClient
    private val vinylViewModel: VinylViewModel by activityViewModels() {
        VinylViewModel.factory(repository, this)
    }

    companion object{
        private const val LOCATION_REQUEST_CODE = 100
    }

    private val binding get() = _binding!!


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentMapBinding.inflate(inflater, container, false)

        createMap()
        return binding.root




    }
    private fun createMap(){
        val mapFragment = childFragmentManager.findFragmentById(R.id.fragment_map) as SupportMapFragment?
        mapFragment?.getMapAsync(this)
        fusedLocationClient = LocationServices.getFusedLocationProviderClient(this.requireContext())
    }
    override fun onMapReady(googleMap: GoogleMap) {
        map = googleMap
        onMapClick()
        map.uiSettings.isZoomControlsEnabled = true
        map.setOnMarkerClickListener(this)
        setUpMap()
        loadMarkers()
        loadSeleted()
    }

    private fun loadMarkers() = lifecycleScope.launch {

        vinylViewModel.vinylList.collect{ vinylList ->
            vinylList.forEach {
                val bitmap =
                    it.image?.let { it1 -> BitmapFactory.decodeByteArray(it.image, 0, it1.size) }
                map.addMarker(MarkerOptions().position(LatLng(it.latitude, it.longitude)).title(it.title).snippet(it.description).icon(bitmap?.let { it1 -> BitmapDescriptorFactory.fromBitmap(
                    Bitmap.createScaledBitmap(it1, 120, 120, false)) }))
            }
        }
    }

    private fun loadSeleted() = lifecycleScope.launch{
        vinylViewModel.selectedVinyl.collect{
            if (it != null) {
                map.animateCamera(
                    CameraUpdateFactory.newLatLngZoom(
                        LatLng(
                            it.latitude,
                            it.longitude
                        ), 15f
                    )
                )
            }
        }
    }

    private fun setUpMap() {
        if (this.context?.let {
                ActivityCompat.checkSelfPermission(
                    it,
                    android.Manifest.permission.ACCESS_FINE_LOCATION
                )
            } != PackageManager.PERMISSION_GRANTED) {

            ActivityCompat.requestPermissions(
                this.context as Activity, arrayOf(android.Manifest.permission.ACCESS_FINE_LOCATION),
                MapFragment.LOCATION_REQUEST_CODE
            )

            return
        }
        map.isMyLocationEnabled = true


        }


    override fun onMarkerClick(p0: Marker) = false

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    private fun onMapClick(){
        map.setOnMapClickListener { latlng ->
            val addMarkerFragment = AddMarkerFragment(latlng)
            activity?.let { addMarkerFragment.show(it.supportFragmentManager, "") }
        }
    }

}