package dev.isagood.caiogg.vinylapp.ui.map

import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.findNavController
import dev.isagood.caiogg.vinylapp.R
import dev.isagood.caiogg.vinylapp.databinding.FragmentMarkerListBinding
import dev.isagood.caiogg.vinylapp.viewmodel.VinylViewModel
import kotlinx.coroutines.launch

class MarkerFragment : Fragment() {

    private var _binding: FragmentMarkerListBinding? = null
    private var columnCount = 1
    private val vinylViewModel: VinylViewModel by activityViewModels() {
        VinylViewModel.factory(repository, this)
    }

    private val binding get() = _binding!!



    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentMarkerListBinding.inflate(inflater, container, false)


        with(binding.fragmentList) {
                lifecycleScope.launch {
                    vinylViewModel.vinylList.collect(){ vinylList ->
                        layoutManager = LinearLayoutManager(context)
                        adapter = VinylAdapter(vinylList){
                            vinylViewModel.changeSelectedVinyl(it)
                            (activity as MapActivity).navigate(MapFragment())
                        }
                    }
                }
        }

        return binding.root
    }

}