package dev.isagood.caiogg.vinylapp.viewmodel

import android.os.Bundle
import androidx.lifecycle.*
import androidx.savedstate.SavedStateRegistryOwner
import dev.isagood.caiogg.vinylapp.data.RealmSyncRepository
import dev.isagood.caiogg.vinylapp.domain.Vinyl
import io.realm.kotlin.notifications.InitialResults
import io.realm.kotlin.notifications.ResultsChange
import io.realm.kotlin.notifications.UpdatedList
import io.realm.kotlin.notifications.UpdatedResults
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch

class VinylViewModel (
    private val repository: RealmSyncRepository,
) : ViewModel() {

    private val _vinylList: MutableLiveData<MutableList<Vinyl>> = MutableLiveData(mutableListOf())
    val vinylList: Flow<List<Vinyl>> get() = _vinylList.asFlow()
    private val _selectedVinyl: MutableLiveData<Vinyl> = MutableLiveData(null)
    val selectedVinyl: Flow<Vinyl?> get() = _selectedVinyl.asFlow()

    init {
        viewModelScope.launch {
            repository.getVinylList().collect(){ event: ResultsChange<Vinyl> ->
                    when(event){
                        is InitialResults -> {
                            _vinylList.value = _vinylList.value?.apply {
                                clear()
                                addAll(event.list)
                            }
                        }
                        is UpdatedResults -> {
                            if (event.insertions.isNotEmpty()) {
                                event.insertions.forEach {
                                    _vinylList.value = _vinylList.value?.apply {
                                        add(it, event.list[it])
                                    }
                                }
                            }
                        }
                        else -> Unit
                    }
            }
        }
    }

    fun changeSelectedVinyl(vinyl: Vinyl){
        _selectedVinyl.value = vinyl
    }


    @Suppress("UNCHECKED_CAST")
    companion object {
        fun factory(
            repository: RealmSyncRepository,
            owner: SavedStateRegistryOwner,
            defaultArgs: Bundle? = null,
        ): AbstractSavedStateViewModelFactory {
            return object : AbstractSavedStateViewModelFactory(owner, defaultArgs) {
                override fun <T : ViewModel> create(
                    key: String,
                    modelClass: Class<T>,
                    handle: SavedStateHandle,
                ): T {
                    return VinylViewModel(repository) as T
                }
            }
        }
    }
}