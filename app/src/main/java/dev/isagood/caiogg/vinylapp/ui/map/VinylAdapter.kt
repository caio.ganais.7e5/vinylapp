package dev.isagood.caiogg.vinylapp.ui.map

import android.graphics.BitmapFactory
import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import dev.isagood.caiogg.vinylapp.databinding.FragmentMarkerBinding
import dev.isagood.caiogg.vinylapp.domain.Vinyl


class VinylAdapter(
    private val values: List<Vinyl>,
    private val listener: (Vinyl) -> Unit
) : RecyclerView.Adapter<VinylAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {

        return ViewHolder(
            FragmentMarkerBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            ).root
        )

    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = values[position]
        with(holder){
            setListener(item)
            idView.text = item.title
            contentView.text = item.description
            imageView.setImageBitmap(BitmapFactory.decodeByteArray(item.image,0,item.image!!.size))
        }

    }


    override fun getItemCount(): Int = values.size

    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val binding = FragmentMarkerBinding.bind(view)
        val idView: TextView = binding.titleList
        val contentView: TextView = binding.descriptionList
        val imageView: ImageView = binding.imagePlace
        fun setListener(vinyl: Vinyl){
            binding.root.setOnClickListener {
                listener(vinyl)
            }
        }


        override fun toString(): String {
            return super.toString() + " '" + contentView.text + "'"
        }
    }


}