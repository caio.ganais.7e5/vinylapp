package dev.isagood.caiogg.vinylapp.ui.map

import android.app.Dialog
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.Toast
import androidx.activity.result.PickVisualMediaRequest
import androidx.activity.result.contract.ActivityResultContracts
import androidx.activity.viewModels
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.ViewModelProvider
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import dev.isagood.caiogg.vinylapp.databinding.FragmentAddMarkerBinding
import dev.isagood.caiogg.vinylapp.viewmodel.AddVinylViewModel


class AddMarkerFragment(val latlng: LatLng) : DialogFragment() {

    private var _binding: FragmentAddMarkerBinding? = null
    private val binding get() = _binding!!
    private val addVinylViewModel: AddVinylViewModel by activityViewModels() {
        AddVinylViewModel.factory(repository, this)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View {
        _binding = FragmentAddMarkerBinding.inflate(inflater, container, false)

        return binding.root
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val dialog = super.onCreateDialog(savedInstanceState)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setCanceledOnTouchOutside(false)
        return dialog
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val buttonCreate =  binding.createButton
        val buttonCancel =  binding.cancelButton
        val photoButton =  binding.cameraButton

        photoButton.setOnClickListener {
            (activity as MapActivity).pickMedia.launch(
                PickVisualMediaRequest(
                ActivityResultContracts.PickVisualMedia.ImageOnly)
            )
        }

        uriLiveData.observe(viewLifecycleOwner){
            photoButton.setImageURI(it)
        }


        buttonCreate.setOnClickListener {
            val title = binding.titleInput.text.toString()
            val description = binding.etDescription.text.toString()
            if (title.trim().isEmpty() || description.trim().isEmpty()){
                Toast.makeText(this.context, "Place must have non-empty title and description", Toast.LENGTH_LONG).show()
                return@setOnClickListener

            } else {

                uriLiveData.value?.let { image ->
                    context?.contentResolver?.openInputStream(image)?.readBytes()
                        ?.let { imageConverted ->
                            addVinylViewModel.addVinyl(
                                title,
                                latlng,
                                description,
                                imageConverted
                            )
                        }
                }

                }
//            val marker = map.addMarker(MarkerOptions().position(latlng).title(title).snippet(description))


            map.animateCamera(
                CameraUpdateFactory.newLatLngZoom(latlng,15f),
                1000,
                null
            )
            dialog?.dismiss()
        }
        buttonCancel.setOnClickListener {
            dialog?.dismiss()
        }

    }

}