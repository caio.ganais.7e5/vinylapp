package dev.isagood.caiogg.vinylapp.ui.map

import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.activity.result.contract.ActivityResultContracts
import androidx.fragment.app.Fragment
import androidx.lifecycle.MutableLiveData
import com.google.android.material.navigation.NavigationBarView
import dev.isagood.caiogg.vinylapp.R
import dev.isagood.caiogg.vinylapp.data.RealmSyncRepository
import dev.isagood.caiogg.vinylapp.databinding.ActivityMapBinding


val repository = RealmSyncRepository()
val uriLiveData = MutableLiveData<Uri?>()



class MapActivity : AppCompatActivity(){

    val pickMedia = registerForActivityResult(ActivityResultContracts.PickVisualMedia()){ uri ->
        if (uri != null){
            uriLiveData.value = uri
            Log.i("a","selected")
        }
        else {
            Log.i("a","No selected")
        }

    }




    private lateinit var binding: ActivityMapBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMapBinding.inflate(layoutInflater)
        val view  = binding.root
        setContentView(view)

        val navView: NavigationBarView = binding.navMenuRail


        navView.setOnItemSelectedListener {item ->
            when(item.itemId){
                R.id.fragment_map -> {
                    navigate(MapFragment())
                    true
                }
                R.id.saved -> {
                    navigate(MarkerFragment())
                    true
                }
                R.id.logout -> {
                    navigate(LogoutFragment())
                    true
                }
                else -> false
            }


        }



    }


    fun navigate(fragment: Fragment){
        val navController = binding.navHostFragment
        val transaction = supportFragmentManager.beginTransaction()
        transaction.replace(navController.id, fragment)
        transaction.commit()
    }



}