package dev.isagood.caiogg.vinylapp.ui.login

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.activity.viewModels
import androidx.navigation.NavHost
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.ui.setupActionBarWithNavController
import dev.isagood.caiogg.vinylapp.app
import dev.isagood.caiogg.vinylapp.databinding.ActivityLoginBinding
import dev.isagood.caiogg.vinylapp.ui.map.MapActivity
import dev.isagood.caiogg.vinylapp.viewmodel.LoginViewModel


class LoginActivity : AppCompatActivity() {

    private lateinit var binding: ActivityLoginBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityLoginBinding.inflate(layoutInflater)
        val view  = binding.root
        setContentView(view)

        if (app.currentUser != null) {
            startActivity(Intent(this, MapActivity::class.java))
            finish()
            return
        }
    }
}